package level;

import tilegame.Game;
import tiles.Tile;

public class LevelData {

	//Tile Management
	private Tile[] tiles;
	private int index=0;
	public int currentWorld = 1;

	public LevelData(Game game) {
		tiles = new Tile[2000];
	}
	
	public Tile[] getTiles() {
		return tiles;
	}

	public void setTiles(Tile tile) {
		tiles[index] = tile;
		index++;
	}

	public void reset() {
		index=0;
	}
}
