package level;

import tilegame.Game;
import tiles.Tile;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class World {

	private Game game;
	private int x;
	private int y;
	private String path;


	public World(Game game) {
		this.game = game;
	}

	private int currentWorld[][] = new int[30][200];

	private void readWorld(){
		int x = 0, y = 0;
		path = "res/worlds/world" + game.getLevelData().currentWorld + ".txt";
		try {
			BufferedReader in = new BufferedReader(new FileReader(path));
			String line;
			while ((line = in.readLine()) != null) {
				String[] values = line.split(",");
				for (String str : values) {
					int txtInt = Integer.parseInt(str);
					currentWorld[x][y] = txtInt;
					y = y + 1;
				}
				y = 0;
				x = x + 1;
			}
			in.close();
		} catch(IOException e) {
			System.out.println("Fehler beim Einlesen von Weltdatei: " + e);
		}

	}	

	public void renderWorld(Graphics g){
		readWorld();
		game.getLevelData().reset();
		for (int i = 0; i < currentWorld.length;i++) {
			for (int k = 0; k < currentWorld[i].length;k++) {
				x = (int) (k * Tile.TILEWIDTH-game.getCamera().getxOffset());
				y = (int) (i * Tile.TILEHEIGHT-game.getCamera().getyOffset());

				//Stone
				if (currentWorld[i][k] == 1) {
					game.getLevelData().setTiles(new Tile(k*32, i*32, 0));
					Tile.tiles[0].render(g, x,y);
				}

				//Grass
				if (currentWorld[i][k] == 2) {
					game.getLevelData().setTiles(new Tile(k*32, i*32, 0));
					Tile.tiles[2].render(g, x,y);
				}

				//Dirt
				if (currentWorld[i][k] == 3) {
					game.getLevelData().setTiles(new Tile(k*32, i*32, 0));
					Tile.tiles[3].render(g,x,y);
				}

				//Brick
				if (currentWorld[i][k] == 4) {
					game.getLevelData().setTiles(new Tile(k*32, i*32, 0));
					Tile.tiles[1].render(g,x,y);
				}

			}
		}
	}

}
