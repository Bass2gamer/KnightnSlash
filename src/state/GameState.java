package state;

import entity.creature.Creature;
import entity.creature.Player;
import entity.creature.TestEnemy;
import entity.worldObject.Portal;
import level.World;
import tilegame.Game;

import java.awt.*;

public class GameState extends State {
	
	private World world;
	private Player player;
	private TestEnemy enemy;
	private Portal portal, portal2, portal3;
	private Game game;

	//Add Objects to Frame
	public GameState(Game game) {
		super(game);
		this.game = game;
		world = new World(game);
		player = new Player(game,200,400 , 200);
		enemy = new TestEnemy(game, 1000,500, 100);
		//portal = new Portal(game, 32, 800, true);
		portal = new Portal(game, 6272, 800, true);
		portal2 = new Portal(game, 7000, 200, false);
		portal3 = new Portal(game, 4000,4000, false);
		game.getCreatures().add(player);
		game.getCreatures().add(enemy);

		game.getPortals()[0] = portal;
		game.getPortals()[1] = portal2;
		game.getPortals()[2] = portal3;
	}
	
	public void tick() {
		for (Creature creature : game.getCreatures()) {
			creature.tick();
		}
	}

	public void render(Graphics g) {
		world.renderWorld(g);
		portal.render(g);
		portal2.render(g);
		portal3.render(g);
		for (Creature creature : game.getCreatures()) {
			creature.render(g);
		}
	}	
}
