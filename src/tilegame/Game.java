package tilegame;

import display.Display;
import entity.creature.Creature;
import entity.worldObject.Portal;
import grafics.Assets;
import grafics.Camera;
import input.KeyManager;
import level.LevelData;
import state.GameState;
import state.State;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;


public class Game implements Runnable {

	private Display display;
	public int width, height;
	public String title;

	private boolean running = false;
	private Thread thread;

	private BufferStrategy bs;
	private Graphics g;

	private State gameState;
	//private State menuState;

	private KeyManager keyManager;

	private Camera camera;
	private LevelData levelData;

	protected Portal[] portal;
	protected ArrayList<Creature> creatures;

	public Game(String title, int width, int height ) {
		this.width = width;
		this.height = height;
		this.title = title;
		keyManager = new KeyManager();
		levelData = new LevelData(this);
		portal = new Portal[3];
		creatures = new ArrayList<Creature>();
	}


	private void init() {	
		display = new Display(title, width, height);
		display.getFrame().addKeyListener(keyManager);
		Assets.init();

		camera = new Camera(this,0,0);

		gameState = new GameState(this);
		//	menuState = new MenuState(this);
		State.setState(gameState);
	}

	public void run() {
		init();
		int sleep=10;
		long start=0;
		while(running) {			
			start=System.currentTimeMillis();
			tick();
			render();
			long timeCalc=System.currentTimeMillis()-start;
			//System.out.println("calcTime: " + (timeCalc));
			try {
				if (sleep-(timeCalc)>0) {
					Thread.sleep(sleep-(timeCalc));
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		stop();
	}

	public KeyManager getKeyManager() {
		return keyManager;
	}

	public Camera getCamera() {
		return camera;
	}

	public Graphics getG() {
		return g;
	}

	public LevelData getLevelData() {
		return levelData;
	}

	public Portal[] getPortals() {
		return portal;
	}

	public ArrayList<Creature> getCreatures() {
		return creatures;
	}

	private void tick() {
		keyManager.tick();

		if(State.getState() != null) {
			State.getState().tick();
		}
	}

	private void render() {
		bs = display.getCanvas().getBufferStrategy();
		if (bs == null) {
			display.getCanvas().createBufferStrategy(2);
			return;
		}
		g = bs.getDrawGraphics();
		g.clearRect(0, 0, width, height);

		if(State.getState() != null) {
			State.getState().render(g);
		}

		bs.show();
		g.dispose();
	}

	public synchronized void start() {
		if (running) {
			return;
		}
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public synchronized void stop() {
		if (!running) {
			return;
		}
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
