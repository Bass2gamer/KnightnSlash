package tiles;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Tile {
	
	public static Tile[] tiles = new Tile[20];
	public static Tile stoneTile = new StoneTile(0);
	public static Tile brick = new BrickTile(1);
	public static Tile gras = new GrassTile(2);
	public static Tile erde = new DirtTile(3);

	public static final int TILEWIDTH = 32, TILEHEIGHT = 32;
	
	protected BufferedImage texture;
	protected final int id;
	
	protected int x, y;

	public Tile(BufferedImage texture, int id) {
		this.texture = texture;
		this.id = id;
		
		tiles[id] = this;
	}
	
	public Tile(int x, int y, int id) {
		this.x = x;
		this.y = y;
		this.id = id;
	}

	public void tick() {

	}
	
	public void render(Graphics g, int x, int y) {
		g.drawImage(texture, x, y, TILEWIDTH, TILEHEIGHT, null);
		this.x = x;
		this.y = y;
	}

	public int getId() {
		return id;
	}
	
	protected boolean isSolid(){
		return true;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public int getWidth() {
		return TILEWIDTH;
	}
	
	public int getHeight() {
		return TILEHEIGHT;
	}

}
