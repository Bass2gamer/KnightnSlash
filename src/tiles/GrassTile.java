package tiles;

import grafics.Assets;

public class GrassTile extends Tile{

    public GrassTile(int id) {
        super(Assets.gras, id);
    }

}
