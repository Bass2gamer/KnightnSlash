package grafics;

import java.awt.image.BufferedImage;

public class Assets {

	public static BufferedImage boden;
	public static BufferedImage gras;
	public static BufferedImage erde;
	public static BufferedImage brick;
	public static BufferedImage pink;
	public static BufferedImage blue;
	public static BufferedImage lightblue;
	public static BufferedImage green;
	public static BufferedImage yellow;
	public static BufferedImage charakter;
	public static BufferedImage portal;
	public static BufferedImage monster;


	private static final int width = 32, height = 32, charHeight = 64;


	public static void init() {

		SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("/textures/textures_neu.png"));

		//Graphics
		charakter = ImageLoader.loadImage("/textures/charakter_standart.png");
		portal = ImageLoader.loadImage("/textures/Portal.png");
		monster = ImageLoader.loadImage("/textures/Monster.png");

		//SpriteSheet Crop
		boden = sheet.crop(0,0, width, height);
		gras = sheet.crop(width, 0, width, height);
		erde = sheet.crop(width*2, 0, width, height);
		brick = sheet.crop(0, height, width, height);
		pink = sheet.crop(width, height, width, height);
		blue = sheet.crop(width*2, height, width, height);
		lightblue = sheet.crop(0, height*2, width, height);
		green = sheet.crop(width, height*2, width, height);
		yellow = sheet.crop(width*2, height*2, width, height);

	}

}
