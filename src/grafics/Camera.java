package grafics;

import entity.Entity;
import tilegame.Game;

public class Camera {

	private float xOffset,yOffset;
	private Game game;
	private final int maxWidth = 6400;


	public Camera(Game game,float xOffset, float yOffset) {
		this.game = game;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}

	public void move(float x, float y) {
		xOffset += x;
		yOffset += y;
	}

	//Kamera Movement auf Player wenn außerhalb von Spielraum
	public void centerOnEntity(Entity entity) {
		if (!((entity.getX()<=630) || (entity.getX() >=(maxWidth-630)))) {
			xOffset = entity.getX() - (game.width / 2);
		}
		if (!(entity.getY()>= 625)) {
			yOffset = entity.getY() - (game.height / 2);
		}
	}

	public float getxOffset() {
		return xOffset;
	}

	public void setxOffset(float xOffset) {
		this.xOffset = xOffset;
	}

	public float getyOffset() {
		return yOffset;
	}

	public void setyOffset(float yOffset) {
		this.yOffset = yOffset;
	}
}
