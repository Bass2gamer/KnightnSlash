package entity.creature;

import entity.Entity;
import tilegame.Game;
import tiles.Tile;

public abstract class Creature extends Entity {

	protected int health;
	protected float speed;
	protected float xMove = 0, yMove = 0;
	protected double gravity = 0.5, maxSpeed = 15;
	protected boolean falling = false, jump = true;
	protected double maxJump = 20, curentJump = 0;

	public static final int DEFAULT_HEALH = 10;
	public static final float DEFAULT_SPEED = 5;
	public static final int DEFAULT_CREATURE_WIDTH = 64, DEFAULT_CREATURE_HEIGHT = 96;

	public Creature(Game game,float x, float y, int width, int height ,int health) {
		super(game,x, y, width, height);
		health = DEFAULT_HEALH;
		speed = DEFAULT_SPEED;
	}

	//Kreatur
	public void move() {
		if(!hasHorizontalCollision()) {
			x += xMove;
		}
		if(!hasVertikalCollision()) {
			y += yMove;
		}
	}

	//Vertikaler Kollisions Check
	public boolean hasVertikalCollision() {
		for(int i = 0; i < game.getLevelData().getTiles().length;i++) {
			if(game.getLevelData().getTiles()[i]!=null) {
				Tile t = game.getLevelData().getTiles()[i];
				if(getBounds((int)x,(int)y,width,height).intersects(getTop(t.getX(),t.getY(),t.getWidth(),t.getHeight())) && yMove >0) {
					jump = true;
					falling = false;
					y = t.getY()- DEFAULT_CREATURE_HEIGHT;
					yMove = 0;
					return true;
				} else {
					falling = true;
				}
				if(getBounds((int)x,(int)y,width,height).intersects(getBottom(t.getX(),t.getY(),t.getWidth(),t.getHeight())) && yMove <0) {
					y = t.getY() + t.getHeight();
					yMove = 0;
					return true;
				}
			}
		}
		return false;
	}

	//Horizontaler Kollisionscheck
	public boolean hasHorizontalCollision() {
		for(int i = 0; i < game.getLevelData().getTiles().length;i++) {
			if(game.getLevelData().getTiles()[i]!=null) {

				Tile t = game.getLevelData().getTiles()[i];
				if(getBounds((int)x,(int)y,width,height).intersects(getRight(t.getX(),t.getY(),t.getWidth(),t.getHeight())) && xMove <0) {
					xMove = 0;
					return true;
				}
				if(getBounds((int)x,(int)y,width,height).intersects(getLeft(t.getX(),t.getY(),t.getWidth(),t.getHeight())) && xMove >0) {
					xMove = 0;
					return true;
				}
			}
		}
		return false;
	}

	protected void fall() {
		yMove +=gravity;
		if(yMove > maxSpeed) {
			yMove = (float) maxSpeed;
		}
	}

	protected void jump(double jumpAcc) {
		if(jump) {
			yMove -= jumpAcc;
			curentJump += jumpAcc;
			if(curentJump >= maxJump) {
				jump = false;
				curentJump = 0;
			}
		}
	}

	public float getxMove() {
		return xMove;
	}

	public void setxMove(float xMove) {
		this.xMove = xMove;
	}

	public float getyMove() {
		return yMove;
	}

	public void setyMove(float yMove) {
		this.yMove = yMove;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}
}