package entity.creature;

import grafics.Assets;
import tilegame.Game;

import java.awt.*;

public class TestEnemy extends Creature {

	private int counter = 0;

	public TestEnemy(Game game, float x, float y, int health) {
		super(game, x, y, 64, 98, health);
	}

	public void tick() {
		fall();
		walkRandom();
		counter++;
		move();
	}

	public void render(Graphics g) {
		g.drawImage(Assets.monster, (int) (x-game.getCamera().getxOffset()), (int) (y-game.getCamera().getyOffset()), width, height, null);
	}

	//Random Movement für Gegner
	public void walkRandom() {
		if (counter == 50) {
			int random = (int) (Math.random() *4);
			if (random == 1) {
				xMove = 1;
			}
			if (random == 0) {
				xMove = -1;
			}
			if (random == 2) {
				xMove = 0;
			}
			if (random == 3) {
				jump(20);
			}
			counter = 0;
		}
	}
}
