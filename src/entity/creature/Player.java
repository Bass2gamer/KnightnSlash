package entity.creature;

import entity.worldObject.Portal;
import grafics.Assets;
import tilegame.Game;

import java.awt.*;

public class Player extends Creature {

	private boolean left = true;

	public Player(Game game,float x, float y, int health) {
		super(game,x, y, 64, 98, health);
	}

	public void tick() {	
		fall();
		getInput();
		move();
		checkPortal();
		game.getCamera().centerOnEntity(this);
	}

	//Key Inputs for Movement
	private void getInput() {
		xMove = 0;
		if(game.getKeyManager().space) {
			jump(2);
		}
		if(game.getKeyManager().left) {
			xMove = -speed;
			left = true;
		}
		if(game.getKeyManager().right) {
			xMove = speed;
			left = false;
		}
	}

	//Player Renderer
	public void render(Graphics g) {
		if(left) {
			g.drawImage(Assets.charakter, (int) (x-game.getCamera().getxOffset()), (int)(y-game.getCamera().getyOffset()), width, height, null);	
		} else {
			g.drawImage(Assets.charakter, (int) ((x + width)-game.getCamera().getxOffset()), (int)(y-game.getCamera().getyOffset()), -width, height, null);
		}
	}

	//Überprüfe auf Portalkollision > Wenn Kollision rechts dann vorherige Welt sonst nächste
	protected void checkPortal() {
		for(int i = 0; i <game.getPortals().length; i++) {
			Portal p = game.getPortals()[i];
			if (getBounds((int)x,(int)y,width,height).intersects(game.getPortals()[i].getBounds((int)p.getX(),(int) p.getY(),p.getWidth(),p.getHeight()))) {
				if (p.getAdd() == true) {
					if (game.getLevelData().currentWorld ==1) {
						game.getLevelData().currentWorld += 1;
					}
				} else {
					if (game.getLevelData().currentWorld ==2) {
						game.getLevelData().currentWorld -= 1;
					}
				}
			}
		}
	}

}