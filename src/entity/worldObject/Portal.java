package entity.worldObject;

import grafics.Assets;
import tilegame.Game;

import java.awt.*;

public class Portal extends WorldObject {

	protected boolean add;

	public Portal(Game game, float x, float y, boolean add) {
		super(game, x, y, 64, 98);
		this.add = add;
	}

	public void tick() {
		
	}

	public void render(Graphics g) {
		g.drawImage(Assets.portal,(int) (x-game.getCamera().getxOffset()),(int) (y-game.getCamera().getyOffset()), 64, 98, null);
	}

	public boolean getAdd() {
		return add;
	}
}
