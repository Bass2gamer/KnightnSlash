package entity.worldObject;

import entity.Entity;
import tilegame.Game;

import java.awt.*;

public abstract class WorldObject extends Entity {

	public WorldObject(Game game, float x, float y, int width, int height) {
		super(game, x, y, width, height);
		
	}

	public void tick() {
		
	}

	public void render(Graphics g) {
	
	}

}

