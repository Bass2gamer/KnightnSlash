package entity;

import tilegame.Game;

import java.awt.*;

public abstract class Entity {

	protected Game game;
	protected float x, y;
	protected int width, height;
	
	public Entity(Game game,float x, float y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.game = game;
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public abstract void tick();
	
	public abstract void render(Graphics g);
	
	public Rectangle getBounds( int x, int y , int width, int height) {
		return new Rectangle((int)x,(int) y, width, height);
	}
	public Rectangle getTop(int x, int y , int width, int height) {
		return new Rectangle((int)x+6,(int) y, width-12, 5);
	}
	public Rectangle getBottom(int x, int y , int width, int height) {
		return new Rectangle((int)x+6, (int)y+height -5, width-12, 5);
	}
	public Rectangle getRight(int x, int y , int width, int height) {
		return new Rectangle((int)x +width-5,(int) y+3, 5, height-6);
	}
	public Rectangle getLeft(int x, int y , int width, int height) {
		return new Rectangle((int)x, (int)y+3, 5, height-6);
	}
}
